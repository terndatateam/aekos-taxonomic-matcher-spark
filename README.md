This project is a mess. The `Main.scala` file will run when you send it to `scala-shell` but this project cannot be built as a JAR and submitted to Spark.

**TODO** follow the steps in https://medium.com/@mrpowers/creating-a-spark-project-with-sbt-intellij-sbt-spark-package-and-friends-cc9108751c28 to generate a Spark compatible project and migrate our code into that.

## Running this code
 1. extract the body of `Main.scala`, so you just have a text file of commands (including the imports), and save it as `blah.txt`
 1. download the `spark-2.3.0-bin-hadoop2.7` binary (or similar) and extract it
 1. create a helper script, `spark.sh`, in the root of the extracted spark binary directory:
    ```bash
    #!/bin/bash
    cd `dirname "$0"`
    export SPARK_HOME=`pwd`
    export PATH=$PATH:$SPARK_HOME
    ./bin/spark-shell
    ```
 1. make the script runnable
    ```bash
    chmod +x spark-2.3.0-bin-hadoop2.7/spark.sh
    ```
 1. run our script using `scala-shell`
    ```bash
    cat blah.txt | spark-2.3.0-bin-hadoop2.7/spark.sh
    ```

## Some notes on Spark for my future self
We're using `DataFrame`s. `DataFrame`s are just a `Dataset` of `Row`s. We have a demo of reading a `DataFrame` from a single table in the H2. We define a UDF and create a new column in the `DataFrame`.

We stil need to figure out how to 1) add a new table, and 2) write the whole thing somewhere. That could be back to an H2 or some other JDBC compatible database. https://spark.apache.org/docs/2.3.0/sql-programming-guide.html will probably have some help.
