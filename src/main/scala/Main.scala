import org.apache.spark.sql.functions.udf
import java.util.Properties

object Main {
  def main(args: Array[String]) {
    val jdbcUrl = s"jdbc:h2:/zeta/git/aekos-ingestion-data/TERN_AUSPLOTS/Data/data"

    // Create a Properties() object to hold the parameters.
    val connectionProperties = new Properties()

    // connectionProperties.put("user", s"${jdbcUsername}")
    // connectionProperties.put("password", s"${jdbcPassword}")

    // Class.forName("org.h2.Driver")
    // import java.sql.DriverManager
    // val connection = DriverManager.getConnection(jdbcUrl)
    // connection.isClosed()
    val speciesTableDf = spark.read.jdbc(jdbcUrl, "R_SPECIESDISTINCTPERSITE", connectionProperties)
    // speciesTableDf.printSchema

    val lookupFunc = { (unmatchedName:String) =>
      val suffix = "aaa"
      val result = unmatchedName + suffix
      result
    }
    // thanks https://stackoverflow.com/a/37227528/1410035 for the UDF solution
    val lookupUdf = udf(lookupFunc)

    val modifiedSpeciesTableDf = speciesTableDf.withColumn("MATCHED_SPECIES_NAME", lookupUdf(speciesTableDf("HERBARIUM_DETERMINATION")))
    modifiedSpeciesTableDf.show(10)
    // TODO make this a Spark application so we don't have to run it through the spark-shell
    // TODO add another table that contains all the species data
  }
}
